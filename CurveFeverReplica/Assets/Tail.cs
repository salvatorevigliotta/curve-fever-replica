﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
[RequireComponent(typeof(EdgeCollider2D))]
public class Tail : MonoBehaviour
{
    public float pointSpacing = .1f;
    public Transform snake;

    private LineRenderer line;
    private List<Vector2> points;
    private EdgeCollider2D col;

	// Use this for initialization
	void Start ()
    {
        line = GetComponent<LineRenderer>();
        col = GetComponent<EdgeCollider2D>();

        points = new List<Vector2>();

        SetPoints();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(Vector3.Distance(points.Last(), snake.position) > pointSpacing)
            SetPoints();
	}

    private void SetPoints()
    {
        if (points.Count > 1)
            col.points = points.ToArray<Vector2>();

        points.Add(snake.position);

        line.positionCount = points.Count;
        line.SetPosition(points.Count - 1, snake.position);
    }
}
