﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    private bool hasEnded = false;
    public void EndGame()
    {
        if (hasEnded)
            return;

        hasEnded = true;
        StartCoroutine(PlayEndAnimation());
    }

    IEnumerator PlayEndAnimation()
    {
        
        Debug.Log("GameOver!!!");

        yield return new WaitForSeconds(1f);

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
