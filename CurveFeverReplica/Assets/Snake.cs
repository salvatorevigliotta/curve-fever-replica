﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snake : MonoBehaviour
{

    public float Speed = 3f;
    public float RotationSpeed = 200f;

    public string inputAxis = "Horizontal";

    private float horizontal = 0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        horizontal = Input.GetAxisRaw(inputAxis);
	}

    private void FixedUpdate()
    {
        transform.Translate(Vector2.up * Speed * Time.fixedDeltaTime, Space.Self);
        transform.Rotate(Vector3.forward * -horizontal * RotationSpeed * Time.fixedDeltaTime);
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if(col.tag == "KillPlayer")
        {
            Speed = 0f;
            RotationSpeed = 0f;

            GameObject.FindObjectOfType<GameManager>().EndGame();
        }
    }
}
